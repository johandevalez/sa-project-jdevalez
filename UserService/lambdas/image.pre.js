import Image from './../models/image'
import qs from 'querystring'

let getRoutes = (event, callback) => {
  const apiRoute = event.resource
  switch(apiRoute) {

    case '/image/list/{uid}':

      if (!event.pathParameters.uid)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))
      Image.get(event.pathParameters.uid)
      .then((data) => {
        renderHttpResponse(callback, data)
      })
      break
  }
}

let postRoutes = (event, callback) => {
  const apiRoute = event.resource
  let data
  try {
    data = JSON.parse(event.body)
  } catch(err) {
    data = qs.parse(event.body)
  }

  console.log(data, event)
  switch(apiRoute) {

    case '/image':

      Image.addImage(data)
      .then((result) => {
        renderHttpResponse(callback, {key: result})
      })
      break
  }
}

let deleteRoutes = (event, callback) => {
  const apiRoute = event.resource
  switch(apiRoute) {

    case '/image/{key}':

      if (!event.pathParameters.key)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))
      Image.removeImage(event.pathParameters.key)
      .then((data) => {
        renderHttpResponse(callback, {msg: data})
      })
      break
  }
}

let handler = (event, context, callback) => {

  if (event.httpMethod) {
    // const data = {
    //   headers: event.headers,
    //   path: event.resource,
    //   queryParams: event.queryStringParameters,
    //   pathParams: event.pathParameters,
    //   bodyParams: event.body,
    //   method: event.httpMethod
    // }

    const method = event.httpMethod

    if (method === 'OPTIONS') {
      renderHttpResponse(callback, {})
      return
    }

    if (method === 'GET') {

      getRoutes(event, callback)

    }

    if (method === 'POST') {

      postRoutes(event, callback)

    }

    if (method === 'DELETE') {

      deleteRoutes(event, callback)

    }
  }
}

function renderHttpResponse(callback, data, status) {

  if (!status) {
    status = 200
  }

  try {
    data = JSON.stringify(data)
  } catch (e) {
    data = `${data}`
  }

  const response = {
    statusCode: status,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
      'Content-Type': 'application/json'
    },
    body: data
  }
  callback(null, response)
}

function renderHttpError(callback, error) {

  const statusCode = 500
  let response

  if (error) {
    response = typeof error === Error ? error.message : error
  }

  renderHttpResponse(callback, response, statusCode)
}

module.exports = {handler}
