import Profile from './../models/profile'
import Comment from './../models/comment'
import qs from 'querystring'

let getRoutes = (event, callback) => {

  const apiRoute = event.resource

  switch(apiRoute) {

    case '/profile/list':
      let profileList = new Profile()
      profileList.list()
      .then((data) => {
        callback(null, renderHttpResponse(data))
      })
      break

    case '/profile/{uid}':

      if (!event.pathParameters.uid)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))

      let profile = new Profile()
      profile.get(event.pathParameters.uid)
      .then((data) => {
        callback(null, renderHttpResponse(data))
      })
      break

    case '/comment/{cid}':

      // BEWARE, we will consider given id a picture ID even though it is called cid
      // confusing? ...
      // ALSO pid is picture ID, not profile ID
      // confusing? ...
      // so yeah now it is called i(mage)key
      let ikey = event.pathParameters.cid
      if(!ikey)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))

      let comment = new Comment()
      comment.get(ikey)
      .then((data) => {
        callback(null, renderHttpResponse(data))
      })
      break
  }
}

/**
  Handle all post events here
  Post events should have a body property
  The post event body property is in url parameter string format and needs to be parsed
**/
let postRoutes = (event, callback) => {

  const apiRoute = event.resource
  let data
  try {
    data = JSON.parse(event.body)
  } catch(err) {
    data = qs.parse(event.body)
  }

  console.log(data, event)
  switch(apiRoute) {

    case '/profile':

      let profile = new Profile()
      profile.write(data)
      .then((result) => {
        let data = result
        if(result.code)
          data = false
        callback(null, renderHttpResponse({uid: data}))
      })
      break

    case '/comment':

      let comment = new Comment()
      comment.write(data)
      .then((result) => {
        let data = result
        if(result.code)
          data = false
        callback(null, renderHttpResponse({cid: data}))
      })
      break
  }
}

let deleteRoutes = (event, callback) => {

  const apiRoute = event.resource

  switch(apiRoute) {

    case '/profile/{uid}':

      if (!event.pathParameters.uid)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))

      let profile = new Profile()
      profile.remove(event.pathParameters.uid)
      .then((data) => {
        callback(null, renderHttpResponse(data))
      })
      break

    case '/comment/{cid}':

      if(!event.pathParameters.cid)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))

      let comment = new Comment()
      comment.remove(event.pathParameters.cid)
      .then((data) => {
        callback(null, renderHttpResponse(data))
      })
      break
  }
}

let putRoutes = (event, callback) => {

  const apiRoute = event.resource

  switch(apiRoute) {

    case '/profile/{uid}':

      if (!event.pathParameters.uid)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))

      let profile = new Profile()
      profile.write( Object.assign(qs.parse(event.body), {
        uid: event.pathParameters.uid
      }))
      .then((result) => {
        let data = result
        if(result.code)
          data = false
        callback(null, renderHttpResponse(data))
      })
      break

    case '/comment/{cid}':

      if(!event.pathParameters.cid)
        return callback(null, renderHttpError({data:false, msg:'Invalid ID provided'}))

      let comment = new Comment()
      comment.write(Object.assign(qs.parse(event.body), {
        cid: event.pathParameters.cid
      }))
      .then((result) => {
        let data = result
        if(result.code)
          data = false
        callback(null, renderHttpResponse(data))
      })
      break
  }
}

let handler = (event, context, callback) => {

  if (event.httpMethod) {
    // const data = {
    //   headers: event.headers,
    //   path: event.resource,
    //   queryParams: event.queryStringParameters,
    //   pathParams: event.pathParameters,
    //   bodyParams: event.body,
    //   method: event.httpMethod
    // }

    const method = event.httpMethod

    if (method === 'OPTIONS') {
      callback(null, renderHttpResponse({}))
      return
    }

    if (method === 'GET') {

      getRoutes(event, callback)

    }

    if (method === 'POST') {

      postRoutes(event, callback)

    }

    if (method === 'DELETE') {

      deleteRoutes(event, callback)

    }

    if (method === 'PUT') {

      putRoutes(event, callback)

    }
  }
}

function renderHttpResponse(data, status) {

  if (!status) {
    status = 200
  }

  const response = {
    statusCode: status,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }
  return response
}

function renderHttpError(error) {

  const statusCode = 500
  let response

  if (error) {
    response = typeof error === Error ? error.message : error
  }
  try {
    response = JSON.stringify(response)
  } catch (e) {
    response = `${response}`
  }
  renderHttpResponse(response, statusCode)
}

module.exports = {handler}
