import AWS from 'aws-sdk'
import cfg from './../config/config.json'
import uuid from 'uuid/v4'
import Comment from './comment'

// S3

const S3 = new AWS.S3({
  signatureVersion: 'v4',
})

AWS.config.update(cfg.aws)

// model? no way, this is (model)controller! needs refactoring...


const BUCKET_NAME = cfg.s3.bucketName
const BUCKET_URL = cfg.s3.bucketUrl
const IMAGE_PATH = 'profile-pictures'
const IMAGE_TYPES = /\.(jpg|jpeg|png|gif)$/g
const IMAGE_URL_PREFIX = `${BUCKET_URL}`

const DELETE_OBJECT_PARAMS = {
  Bucket: BUCKET_NAME,
  Key: ''
}

const PUT_OBJECT_PARAMS = {
  Bucket: BUCKET_NAME,
  Key: ''
}

// DB

const DB = new AWS.DynamoDB.DocumentClient()

const TABLE_NAME = "pj-jdevalez-picture"

const TABLE_SCHEME = {
	Item: {
		uid: '',
    key: '',
    sort: '',
    name: ''
	},
	TableName: TABLE_NAME
}

const GET_IMAGE_PARAMS = {
	TableName: TABLE_NAME,
	Key: {
    "key": 1
  }
  //ScanIndexForward: false
  //Select: 'COUNT'
}

const QUERY_PARAMS = {
  TableName: TABLE_NAME,
  KeyConditionExpression: "uid = :uid",
  ExpressionAttributeValues: {
    ":uid": "uid"
  }
}

class Image {
  addImage(data) {
    // AWS API GATEWAY does NOT support multipart form data, hoorah!
    // Let the code be, maybe someday it will support it
    let imageKey = uuid()
    let action
    let dbParams = Object.assign({}, TABLE_SCHEME, {
      Item: {
        uid: data.uid,
        key: imageKey,
        sort: Date.now(),
        name: data.filename
      }
    })
    if(data.imagefile) {
      return S3.putObject(Object.assign({}, PUT_OBJECT_PARAMS, {
        Key: `${IMAGE_PATH}/${imageKey}`,
        Body: data.imagefile // buffer
      })).promise()
      .then(() => {
        return DB.put(dbParams).promise()
      })
      .then(() => imageKey)
      .catch((err) => {
        console.log(err)
      })
    } else {
      return DB.put(dbParams).promise()
      .then(() => this.getSignedUrl(`${IMAGE_PATH}/${imageKey}`))
      .catch((err) => {
        console.log(err)
      })
    }
    // after upload, write to db and link image to profile
  }

  removeImage(key) {
    if (!key) {
      throw Error('Please send a file name.')
    }
    const fileToDelete = `${IMAGE_PATH}/${key}`
    const params = Object.assign({}, DELETE_OBJECT_PARAMS, {
      Key: fileToDelete
    })
    return S3.deleteObject(params).promise()
    .then(() => {
      return DB.delete(Object.assign({}, GET_IMAGE_PARAMS, {
        Key: {
          "key": key
        }
      })).promise()
    })
    .then(() => {
      let comment = new Comment()
      comment.removeByImageKey(key)
    })
    .catch((err) => {
      console.log(err)
    })
  }

  get(uid) {
    return DB.query(Object.assign({}, QUERY_PARAMS, {
      IndexName: 'uid-sort-index',
      ExpressionAttributeValues: {
        ':uid': uid
      }
    }))
    .promise()
    .then((result) => result.Items)
    .catch((err) => {
      console.log(err)
    })
  }

  removeByUid(uid) {
    this.get(uid)
    .then((result) => {
      result.map((val) => {
        this.removeImage(val.key)
      })
    })
  }

  getSignedUrl(key) {
    const params = Object.assign({}, PUT_OBJECT_PARAMS, {
      Key: key || uuid()
    })
    return S3.getSignedUrl('putObject', params)
  }
}

export default new Image()
