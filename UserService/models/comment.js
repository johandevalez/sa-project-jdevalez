import cfg from './../config/config.json'
import AWS from 'aws-sdk'
import uuid from 'uuid/v4'

// model? no way, this is modelcontroller! needs refactoring...

AWS.config.update(cfg.aws)

const DB = new AWS.DynamoDB.DocumentClient()

const TABLE_NAME = "pj-jdevalez-comment"

const TABLE_SCHEME = {
	Item: {
    cid: '',
    ikey: '',
		uid: '',
    comment: '',
    liked: 0,
    created: '',
	},
	TableName: TABLE_NAME
}

const COMMENT_PARAMS = {
	TableName: TABLE_NAME,
	Key: {
    "cid": 1
  },
  //ScanIndexForward: false
  //Select: 'COUNT'
}

const QUERY_PARAMS = {
  TableName: TABLE_NAME,
  KeyConditionExpression: "ikey = :ikey",
  ExpressionAttributeValues: {
    ":ikey": "ikey"
  }
}

class Comment {
  constructor() {
    this.data = TABLE_SCHEME.Item
    this.isNew = false
  }
  setCid(cid) {
    if(!cid) {
      this.isNew = true
      cid = uuid()
    }
    this.data.cid = cid
  }
  setUid(uid) {
    this.data.uid = uid
  }
  setImageKey(ikey) {
    this.data.ikey = ikey
  }
  setComment(comment) {
    this.data.comment = comment
  }
  setLiked(liked) {
    this.data.liked = parseInt(liked) === 1? 1 : 0
  }
  setCreated(created) {
    if(this.isNew)
      this.data.created = Date.now()
  }
  get(ikey) {
    if(ikey)
      this.setImageKey(ikey)
    if(!this.data.ikey)
      return {data: false, msg: 'No comment ID provided'}
  	return DB.query(Object.assign({}, QUERY_PARAMS, {
      IndexName: 'ikey-created-index',
      ExpressionAttributeValues: {
        ':ikey': this.data.ikey
      }
    }))
    .promise()
    .then((result) => result.Items || false)
    .catch((err) => {
      console.log(err)
    })
  }
  set(commentData) {
    try {
      this.setCid(commentData.cid)
      this.setUid(commentData.uid)
      this.setImageKey(commentData.ikey)
      this.setComment(commentData.comment)
      this.setLiked(commentData.liked)
      this.setCreated(commentData.created)
    } catch(err) {
      console.error(err)
    }
  }
  write(commentData) {
    if(commentData)
      this.set(commentData)

    if(!this.data.comment)
      return {data: false, msg: 'No valid comment provided'}
    if(this.isNew) {
      return DB.put(Object.assign({}, TABLE_SCHEME, {
    		Item: this.data
    	})).promise()
      .then((result) => {
        return this.data.cid
      })
      .catch((err) => {
        console.log(err)
      })
    } else {
      return DB.update(Object.assign({}, COMMENT_PARAMS, {
    		Key: {
          'cid': this.data.cid
        },
        UpdateExpression: 'set #c=:c, #l=:l',
        ExpressionAttributeNames: {
          '#c' : 'comment',
          '#l' : 'liked'
        },
        ExpressionAttributeValues: {
          ':c': this.data.comment,
          ':l': this.data.liked
        }
        // add cid filter to only update one comment
    	})).promise()
      .then((result) => {
        return this.data
      })
      .catch((err) => {
        console.log(err)
      })
    }

  }
  remove(cid) {
    if(cid)
      this.setCid(cid)

    if(!this.data.cid)
      return {data: false, msg: 'No comment ID provided'}

    return DB.delete(Object.assign({}, COMMENT_PARAMS, {
      Key: {
        'cid': this.data.cid
      }
      // Add condition for cid, to remove only 1
    })).promise()
    .then((result) => {
      return result
    })
    .catch((err) => {
      console.log(err)
    })
  }

  removeByImageKey(ikey) {
    this.get(ikey)
    .then((result) => {
      result.map((val) => {
        this.remove(val.cid)
      })
    })
  }
}

export default Comment
