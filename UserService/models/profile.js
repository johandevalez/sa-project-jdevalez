import cfg from './../config/config.json'
import Image from './image'
import AWS from 'aws-sdk'
import uuid from 'uuid/v4'


// model? no way, this is modelcontroller! needs refactoring...

AWS.config.update(cfg.aws)

const DB = new AWS.DynamoDB.DocumentClient()

const TABLE_NAME = "pj-jdevalez-profile"

const TABLE_SCHEME = {
	Item: {
		uid: '',
    username: '',
    description: '',
	},
	TableName: TABLE_NAME
}

const GET_PROFILE_PARAMS = {
	TableName: TABLE_NAME,
	Key: {
    "uid": 1
  },
  //ScanIndexForward: false
  //Select: 'COUNT'
}

const QUERY_PARAMS = {
  TableName: TABLE_NAME,
  KeyConditionExpression: "ikey = :ikey",
  ExpressionAttributeValues: {
    ":ikey": "ikey"
  }
}

class Profile {
  constructor() {
    this.data = TABLE_SCHEME.Item
    this.isNew = false
  }
  setUid(uid) {
    if(!uid) {
      this.isNew = true
      uid = uuid()
    }
    this.data.uid = uid
  }
  setName(username) {
    this.data.username = username
  }
  setDescription(description) {
    this.data.description = description
  }
  get(uid) {
    if(uid)
      this.setUid(uid)

    if(this.isNew)
      return {data: false, msg: 'No user ID provided'}
  	return DB.get(Object.assign({}, GET_PROFILE_PARAMS, {
      Key: {
        "uid": this.data.uid
      }
    }))
    .promise()
    .then((result) => {
      this.data = result.Item
      return this.data || false
    })
    .catch((err) => {
      console.log(err)
    })
  }
  set(profileData) {
    try {
      this.setUid(profileData.uid)
      this.setName(profileData.username)
      this.setDescription(profileData.description)
    } catch(err) {
      console.error(err)
    }
  }
  write(profileData) {
    if(profileData)
      this.set(profileData)

    if(!this.data.username)
      return {data: false, msg: 'No username provided'}
    if(this.isNew) {
      return DB.put(Object.assign({}, TABLE_SCHEME, {
    		Item: this.data
    	})).promise()
      .then((result) => {
        return this.data.uid
      })
      .catch((err) => {
        console.log(err)
      })
    } else {
      return DB.update(Object.assign({}, GET_PROFILE_PARAMS, {
    		Key: {
          uid: this.data.uid
        },
        UpdateExpression: 'set username=:u, description=:d',
        ExpressionAttributeValues: {
          ':u': this.data.username,
          ':d': this.data.description
        }

    	})).promise()
      .then((result) => {
        return this.data
      })
      .catch((err) => {
        console.log(err)
      })
    }

  }
  remove(uid) {
    if(uid)
      this.setUid(uid)

    if(this.isNew)
      return {data: false, msg: 'No user ID provided'}
    return DB.delete(Object.assign({}, GET_PROFILE_PARAMS, {
      Key: {
        "uid": this.data.uid
      }
    })).promise()
    // remove images
    .then(() => {
      Image.removeByUid(this.data.uid)
    })
    .then((result) => {
      return result
    })
    .catch((err) => {
      console.log(err)
    })
  }

  //

  list() {
    console.log('get data')
    return DB.scan({
      TableName: TABLE_NAME,
      ProjectionExpression: "uid, username"
    }).promise()
    .then((result) => result.Items)
    .catch((err) => {
      console.log(err)
    })
  }
}

export default Profile
