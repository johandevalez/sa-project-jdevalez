import restify from 'restify'
import handlers from './handlers'

// Define a port where the microservice listens for API requests
const PORT = process.env.PORT || 3001

// Init and config the Restify server
const server = restify.createServer()
server.use(restify.bodyParser({
  mapParams: true,
  mapFiles: true
}))
server.use(restify.queryParser())
server.use(restify.CORS())

// Define API routes for profile
// GET
server.get('/profile/list', handlers.profile.getList)
server.get('/profile/:uid', handlers.profile.get)
// DEL
server.del('/profile/:uid', handlers.profile.remove)
// POST - add
server.post('/profile', handlers.profile.add)
// PUT - edit
server.put('/profile/:uid', handlers.profile.edit)

// Define API routes for comment
// GET
server.get('/comment/:pid', handlers.comment.get)
// DEL
server.del('/comment/:cid', handlers.comment.remove)
// POST - add
server.post('/comment', handlers.comment.add)
// PUT - edit
server.put('/comment/:cid', handlers.comment.edit)

// Define API routes for comment
// GET
server.get('/image/list/:uid', handlers.image.list)
// POST
server.post('/image', handlers.image.add)
// DELETE
server.del('/image/:key', handlers.image.remove)

//

server.listen(PORT, ()=> {
  console.log('%s listening at %s', 'UserService', server.url)
})
