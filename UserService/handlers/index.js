import profile from './profile'
import comment from './comment'
import image from './image'

export default {profile, comment, image}
