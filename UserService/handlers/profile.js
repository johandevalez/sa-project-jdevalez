import Profile from './../models/profile'

/**
  Returns a user profile according to given ID
**/
let get = (req, res, next) => {
  if(!req.params.uid)
    return res.json({data:false, msg:'Invalid ID provided'})
  let profile = new Profile()
  profile.get(req.params.uid)
  .then((data) => {
    res.json(data)
  })
}

let getList = (req, res, next) => {
  let profile = new Profile()
  profile.list()
  .then((data) => {
    res.json(data)
  })
}

/**

**/
let remove = (req, res, next) => {
  if(!req.params.uid)
    return res.json({data:false, msg:'Invalid ID provided'})

  let profile = new Profile()
  profile.remove(req.params.uid)
  .then((data) => {
    res.json(data)
  })
}

/**

**/
let add = (req, res, next) => {
  let profile = new Profile()
  profile.write(req.params)
  .then((result) => {
    let data = result
    if(result.code)
      data = false
    res.json({uid: data})
  })
}

/**

**/
let edit = (req, res, next) => {
  if(!req.params.uid)
    return res.json({data:false, msg:'Invalid ID provided'})

  let profile = new Profile()
  profile.write(req.params)
  .then((result) => {
    let data = result
    if(result.code)
      data = false
    res.json(data)
  })
}

export default {
  get,
  getList,
  remove,
  add,
  edit
}
