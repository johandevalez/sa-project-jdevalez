import Comment from './../models/comment'

/**
  Returns a user profile according to given ID
**/
let get = (req, res, next) => {
  if(!req.params.pid)
    return res.json({data:false, msg:'Invalid ID provided'})
  let comment = new Comment()
  comment.get(req.params.pid)
  .then((data) => {
    res.json(data)
  })
}

/**

**/
let remove = (req, res, next) => {
  if(!req.params.cid)
    return res.json({data:false, msg:'Invalid ID provided'})

  let comment = new Comment()
  comment.remove(req.params.cid)
  .then((data) => {
    res.json(data)
  })
}

/**

**/
let add = (req, res, next) => {
  let comment = new Comment()
  comment.write(req.params)
  .then((result) => {
    let data = result
    if(result.code)
      data = false
    res.json({cid: data})
  })
}

/**

**/
let edit = (req, res, next) => {
  if(!req.params.cid)
    return res.json({data:false, msg:'Invalid ID provided'})

  let comment = new Comment()
  comment.write(req.params)
  .then((result) => {
    let data = result
    if(result.code)
      data = false
    res.json(data)
  })
}

export default {
  get,
  remove,
  add,
  edit
}
