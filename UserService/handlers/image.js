import Image from './../models/image'

/**

**/
let add = (req, res, next) => {
  Image.addImage(req.params)
  .then((data) => {
    res.json({key: data})
  })
}

/**

**/
let remove = (req, res, next) => {
  Image.removeImage(req.params.key)
  .then((data) => {
    res.json({msg: data})
  })
}

/**

**/
let list = (req, res, next) => {
  Image.get(req.params.uid)
  .then((data) => {
    res.json(data)
  })
}

export default {
  remove,
  add,
  list
}
