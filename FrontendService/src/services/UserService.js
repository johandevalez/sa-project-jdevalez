import Auth0Lock from 'auth0-lock';
import { browserHistory } from 'react-router'

class UserService {

	constructor() {
		this.lock = new Auth0Lock(
			"yEflx4PblHTKLuIo8BaYs9W2HLA4eFE6",
			'star-academy.eu.auth0.com',
			{
				auth: {
					//redirectUrl: 'http://localhost:3000/login',
					responseType: 'token'
				}
			}
		)

		this.lock.on('authenticated', this._doAuthentication.bind(this))
	}

	showPopUpLogin() {
		this.lock.show();
	}

	userIsLoggedIn() {
		let idToken = localStorage.getItem('id_token');
		return idToken;
	}

	_doAuthentication(resp) {

		this.lock.getProfile(
			resp.idToken,
			(err, response) => {
				localStorage.setItem('id_token', resp.idToken);
				browserHistory.replace('/profile') // redirect to linked user profile
			}
		)
	}

	doLogout() {
		localStorage.removeItem('id_token');
		browserHistory.replace('/home')
	}

}

let instance = new UserService();

export default instance;
