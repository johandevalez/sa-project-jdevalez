import React, { PropTypes, Component} from 'react'
import connect from './../lib/connectComponent'

import UserList from './../components/UserList';


class App extends Component {
	render() {
		return (
			<div>
				{this.props.children}
				<UserList users={this.props.users} getUsersAction={this.props.actions.getUsers} />
			</div>
		);
	}
}

App.propTypes = {
  actions: PropTypes.object.isRequired
}

export default connect(App)
