import * as types from '../constants/ActionTypes'
import axios from 'axios'
import api from './../config/api.conf.json'

const postCfg = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded"
  }
}

export const errorReceived = (err) => ({ type: types.ERROR_RECEIVED, err})
export const appLoading = () => ({ type: types.APP_IN_LOADING_STATE})

export const dataReceived = (data) => ({ type: types.DATA_RECEIVED, data})
export const dataAdd = (data) => ({ type: types.DATA_ADD, data})

export const addUser = (data) => {
  return (dispatch) => {
    dispatch(appLoading())
    return axios.post(`${api.domain}${api.profile.add}`, data, postCfg)
    .then((result) => {
      data.uid = result.data.uid
      dispatch(dataReceived(data))
    })
    .catch((err) => {
      dispatch(errorReceived(err))
    })
  }
}

export const getUsers = () => {
  return (dispatch) => {
    dispatch(appLoading())
    return axios.get(`${api.domain}${api.profile.getList}`)
    .then((result) => {
      dispatch(dataReceived(result.data))
    })
    .catch((err) => {
      dispatch(errorReceived(err))
    })
  }
}

export const getUserImages = (uid) => {
  return (dispatch) => {
    return axios.get(`${api.domain}${api.images.get}`.replace(':uid', uid))
    .then((result) => {
      return result.data
    })
    .catch((err) => {
      console.log(err)
    })
  }
}

export const getProfile = (uid) => {
  return (dispatch) => {
    return axios.get(`${api.domain}${api.profile.get}`.replace(':uid', uid))
    .then((result) => {
      return result.data
    })
    .catch((err) => {
      console.log(err)
    })
  }
}

export const getComments = (key) => {
  return (dispatch) => {
    return axios.get(`${api.domain}${api.comments.get}`.replace(':key', key))
    .then((result) => {
      return result.data
    })
    .catch((err) => {
      console.log(err)
    })
  }
}
