import reducer from "./reducer"
import {createStore, applyMiddleware} from "redux"
import thunk from 'redux-thunk'

let INITAL_STATE = {
	users:[]
}

export let store = createStore(reducer, INITAL_STATE, applyMiddleware(thunk));
