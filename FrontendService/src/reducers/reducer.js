import {
  APP_IN_LOADING_STATE,
  DATA_RECEIVED,
  DATA_ADD,
  ERROR_RECEIVED
} from '../constants/ActionTypes'

export default function reducer(state , action) {
  let newState = JSON.parse(JSON.stringify(state))
  switch (action.type) {
    case DATA_ADD:
      newState.users.push(action.data);
      return newState

    case APP_IN_LOADING_STATE:
       console.log("app in loading state");
       return newState

    case DATA_RECEIVED:
       newState.users = action.data
       return newState

    case ERROR_RECEIVED:
       alert("Error'")
       return newState

    default:
      return state
  }
}
