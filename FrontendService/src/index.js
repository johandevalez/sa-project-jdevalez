import React from 'react'
import ReactDOM from 'react-dom'
import { Router, browserHistory, Route, IndexRoute } from 'react-router'
// Redux
import { Provider } from 'react-redux'
import {store} from "./reducers/store"

import App from './containers/App'
// Components
import Home from './components/Home'
import User from './components/User';
import Profile from './components/Profile'
import Comment from './components/Comment'

import NotFound from './components/NotFound';
import Login from './components/Login';
// Services
// import UserServices from './services/UserService';

import './index.css';

/*
let checkIfLoggedIn = (next, redirect) => {
	if (!UserServices.userIsLoggedIn()) {
		redirect({ pathname: "/login" });
	}
}*/

ReactDOM.render(
  <Provider store={store}>
  	<Router history={browserHistory}>
  		<Route path="/" component={App}>
  			<IndexRoute component={Home}></IndexRoute>
  			<Route path="/home" component={Home}></Route>
  			<Route path="/login" component={Login}></Route>
        <Route path="/profile/:uid" component={User}>
          <IndexRoute component={Profile}></IndexRoute>
          <Route path="/comment/:key" component={Comment}></Route>
        </Route>

  			<Route path="*" component={NotFound}></Route>
  		</Route>
  	</Router>
  </Provider>,
	document.getElementById('root')
);
