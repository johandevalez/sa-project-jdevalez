import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as TodoActions from './../reducers/actions'


const mapStateToProps = state => {
  return {
    users: state.users
  }
}

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(TodoActions, dispatch)
})

const doConnect = (component) => {
  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(component)
}

export default doConnect
