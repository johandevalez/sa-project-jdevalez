import React, { PropTypes, Component } from 'react';
import connect from './../lib/connectComponent'
import { Link } from 'react-router';
import awsCfg from './../config/aws.conf.json'


class User extends Component {
  constructor() {
    super()
    this.state = {
      pingpong: false
    }
    this._images = []
  }
  setImages() {
    this.props.actions.getUserImages(this.props.params.uid)
    .then((images) => {
      this._images = images
      this.setState({
        pingpong: !this.state.pingpong
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }
  renderImage(val, i) {
    return (
      <Link key={i} to={`/comment/${val.key}`}>
        <img src={`${awsCfg.s3.profileImageUrl}${val.key}`} alt={val.name} />
      </Link>
    )
  }
	render() {
    if(this.props.params.uid && this._uid !== this.props.params.uid) {
      this._uid = this.props.params.uid
      this._images = []
      // get user images
      this.setImages()
    }
		return (
			<div className="main">
				<h1>Profile</h1>

        {this.props.children}

        <div className="img-list">
          {this._images.map(this.renderImage)}
        </div>
			</div>
		)
	}
}

User.propTypes = {
  actions: PropTypes.object.isRequired
}

export default connect(User)
