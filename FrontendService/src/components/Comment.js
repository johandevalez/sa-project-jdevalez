import React, { PropTypes, Component } from 'react';
import connect from './../lib/connectComponent'

class Comment extends Component {
  constructor() {
    super()
    this.state = {
      pingpong: false
    }
    this._comments = []
  }
  setComments() {
    this.props.actions.getComments(this.props.params.key)
    .then((comments) => {
      this._comments = comments
      console.log(comments)
      this.setState({
        pingpong: !this.state.pingpong
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }
  renderComment(val, i) {
    return (
      <article key={i}>
        {val.liked?<p>This guy liked the picture!</p>:''}
        {val.comment}
      </article>
    )
  }
	render() {
    if(this.props.params.key && this._key !== this.props.params.key) {
      this._key = this.props.params.key
      this._images = []
      // get picture comments
      this.setComments()
    }
		return (
      <div className="comment-list">
        {this._comments.map(this.renderComment)}
      </div>
		)
	}
}

Comment.propTypes = {
  actions: PropTypes.object.isRequired
}

export default connect(Comment)
