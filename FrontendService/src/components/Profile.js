import React, { PropTypes, Component } from 'react';
import connect from './../lib/connectComponent'


class Profile extends Component {
  constructor() {
    super()
    this.state = {
      pingpong: false
    }
    this._profile = {}
  }
  setProfile() {
    this.props.actions.getProfile(this.props.params.uid)
    .then((profile) => {
      this._profile = profile
      this.setState({
        pingpong: !this.state.pingpong
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }
	render() {
    if(this.props.params.uid && this._uid !== this.props.params.uid) {
      this._uid = this.props.params.uid
      this._profile = {}
      // get user images
      this.setProfile()
    }
		return (
			<div>
        <p>
          Hi my name is {this._profile.username}
        </p>
        <p>
          And this is what I would like you to know about me <br />
          {this._profile.description}
        </p>
			</div>
		)
	}
}

Profile.propTypes = {
  actions: PropTypes.object.isRequired
}

export default connect(Profile)
