import React, { Component } from 'react'
import { Link } from "react-router"

class UserList extends Component {
  componentWillMount() {
    this.props.getUsersAction()
  }
  renderUserLink(val, i) {
    return (
      <Link key={i} to={`/profile/${val.uid}`}>
        {val.username}
      </Link>
    )
  }
	render() {
    
		return (
			<div className="user-list">
				<p>Our users</p>
				{this.props.users.map(this.renderUserLink)}
			</div>
		)
	}
}

export default UserList
