import React, { Component } from 'react'

import UserService from '../services/UserService'

class Login extends Component {

	doLogin() {
		UserService.showPopUpLogin();
	}

	render() {
		return (
			<div>
				<h1>Login</h1>
				<button onClick={this.doLogin.bind(this)}>Sign In</button>
			</div>
		)
	}
}

export default Login